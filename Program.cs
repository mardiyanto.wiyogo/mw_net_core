﻿using System;

namespace dotnetcore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("This is a new line");
        }

        static void MyMethod(){
            Console.WriteLine("Hellow world!");
            // adding method comment
        }
        
        static void Method02(){
            Console.WriteLine("Method 02");
            // comment
        }
    }
    // new line
    // let's see if this change get into changelog
}
